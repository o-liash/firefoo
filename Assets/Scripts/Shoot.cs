﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shoot : MonoBehaviour
{
    public float speed = 20f;
    public int damage = 1;
    public Rigidbody2D rigidBody;
    public AudioClip enemyHitClip;
    private GameObject enemy;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody.velocity = transform.right * speed;
    }

    void Update() {
        if (enemy != null) {
            GameObject.Find("SoundSource").GetComponent<SoundController>().PlayEnemyHit();
            Destroy(enemy);
            IncreaseScore();
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.tag == "Enemy") {   
            enemy = collider.gameObject;
            collider.enabled = false;
        }
    }

    private void KillEnemy(GameObject enemy) {
        StartCoroutine(Fade(enemy));
        Destroy(enemy, 0.2f);  
    }

    private IEnumerator Fade(GameObject enemy) 
    {
        Color c = enemy.GetComponent<Renderer>().material.color;
        c.a = 0.5f;
        for (int i = 0; i < 9; i++) 
        {
             c = enemy.GetComponent<Renderer>().material.color;
            c.a -= 0.1f;
            enemy.GetComponent<Renderer>().material.color = c;
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void IncreaseScore() {
        ++ScoreManager.score;
    }
}
